FROM openjdk:8
MAINTAINER Deepika Muralidharan
WORKDIR /User
COPY . /User
CMD java -jar user-0.0.1-SNAPSHOT.jar
EXPOSE 9050