package com.example.controller;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.constants.ApplicationConstants;
import com.example.entity.UserPOJO;
import com.example.service.UserRegistrationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/user")
@Api(value="Employee Management System")

public class UserRegisterController {

	@Autowired
	UserRegistrationService userRegistrationService;

	
	  @PostMapping(value="/register", produces ="application/json") public String
	  registerUserStatus(@RequestBody UserPOJO user) throws IOException {
	  userRegistrationService.registerUser(user); return
	  ApplicationConstants.SUCCESS;
	  
	  }
	 
	
	@ApiOperation(value = "View list of available employees", response = UserPOJO.class)
	@GetMapping(value="/retriveByNum" , produces="application/json")
	public String retriveUserDetails(@RequestParam(value="phno", required=true) String phno) throws ParseException{
		String JsonString = null;	
		JsonString =userRegistrationService.retriveUserDetails(phno);
		return JsonString;
	}

	
	/*
	 * @ApiOperation(value = "View list of available employees", response =
	 * UserPOJO.class)
	 * 
	 * @GetMapping(value="/retriveAllUsers", produces = "application/json")
	 * 
	 * @ResponseBody public List<UserPOJO>
	 * retriveAllUsers(@RequestParam(value="jobrole")String jobrole)throws
	 * IOException{ List<UserPOJO> users =null; users =
	 * userRegistrationService.retriveAllUsers(jobrole); return users; }
	 */
	
	@ApiOperation(value = "View list of available employees", response = UserPOJO.class)
	@GetMapping(value = "/retriveAllUsers", produces = "application/json")
	@ResponseBody
	 public ResponseEntity<List<UserPOJO>> retriveAllUsers(@RequestParam(value="jobrole",required = false)String jobrole)throws IOException{
		List<UserPOJO> users =null; 
		users =  userRegistrationService.retriveAllUsers(jobrole);
		if(users != null) {
		return new ResponseEntity<List<UserPOJO>>(users, HttpStatus.OK);
		}else
		{
			return new ResponseEntity<>(users,HttpStatus.valueOf("Empty list"));
		}
	}
	
	
	/*
	 * @PutMapping(value="/updateUser", produces="application/json") public String
	 * updateUserDetails(@RequestBody UserPOJO userEntity)throws ParseException {
	 * 
	 * userRegistrationService.up(userEntity); return "SUCCESS";
	 * }
	 */
	
	@ApiOperation(value = "Register new employees", response = UserPOJO.class)
	@PostMapping(value="/registerNewUser", produces="application/json")
	public ResponseEntity<Object> registerUserWithRestriction(@RequestBody UserPOJO userDetails)throws IOException{
		String response = userRegistrationService.registerUserWithRestriction(userDetails);
		if(response.contains("Success")) {
		return new ResponseEntity<>(response,HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@ApiOperation(value = "Uploading Resume", response = UserPOJO.class)
    @PostMapping(value="/uploadFile", consumes = {"multipart/form-data"})
    public byte[] uploadFile(@RequestParam("file") MultipartFile file , @RequestParam(value ="phNum",required = true) String phNum) {
    	byte[] fileName = userRegistrationService.uploadFile(file,phNum);
		
		/*
		 * String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
		 * .path("/downloadFile/") .path(fileName) .toUriString();
		 * return new Response(fileName, fileDownloadUri, file.getContentType(),
		 * file.getSize());
		 */
    	
    	return fileName;
    }
	
	
	@PatchMapping(value ="/updateUser/{id}")
	public String updateUserPhNum(@RequestBody String email ,@PathVariable String id) {
		String res = userRegistrationService.updateUserMailId(email,id);
		return res;
	}
	
}
