package com.example.service;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.example.entity.UserPOJO;

@Component
public interface UserRegistrationService {
 
	public String registerUser(UserPOJO userPojo);
	
	public String retriveUserDetails(String phno);
	
	public String updateUserDetails(UserPOJO userEntity,String id);
	
	public String registerUserWithRestriction(UserPOJO userDetails);
	
	public String updateUserMailId(String email ,String id);
	
	 public byte[] uploadFile(MultipartFile file ,String phNum);
	 
	 public List<UserPOJO> retriveAllUsers(String jobrole);
	 
	
	 
	 
}
