package com.example.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.constants.ApplicationConstants;
import com.example.entity.FileStorageProperties;
import com.example.entity.FileUpload;
import com.example.entity.UserPOJO;
import com.example.exception.UserException;
import com.example.exception.UserNotFoundException;
import com.example.repo.UserRepositary;
import com.example.service.UserRegistrationService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

	@Autowired
	private UserRepositary userRepo;

	private Path fileStorageLocation;

	public UserRegistrationServiceImpl(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			return;
		}
	}

	public byte[] uploadFile(MultipartFile file, String phNum) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		byte[] resume = null;
		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				return null;
			}
			resume = file.getBytes();

			FileUpload fileUp = new FileUpload();
			fileUp.setPhNum(phNum);
			fileUp.setResume(resume);

			userRepo.save(fileUp);

			/**
			 * Path path = Paths.get(fileName); Files.write(path, resume); Copy file to the
			 * target location (Replacing existing file with the same name) Path
			 * targetLocation = this.fileStorageLocation.resolve(fileName);
			 * Files.copy(file.getInputStream(), targetLocation,
			 * StandardCopyOption.REPLACE_EXISTING);
			 */

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return resume;
	}

	public String registerUser(UserPOJO user) {
		userRepo.save(user);
		return ApplicationConstants.SUCCESS;

	}

	// @HystrixCommand(fallbackMethod = "fallbackUser")
	public String retriveUserDetails(String phoneno) {
		// JsonObject responseDetailsJson = new JsonObject();

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		List<UserPOJO> details = userRepo.findbyPhoneNum(phoneno);
		// ObjectMapper objectMapper = new ObjectMapper();
		// LObjectToString = objectMapper.writeValueAsString(details);
		Gson gson = new Gson();
		String json = gson.toJson(details);
		/*
		 * // JSONObject jsonObject = new JSONObject(arrayToJson); //String Jsonresponse
		 * = gson.toJson(arrayToJson); JsonObject obj = new
		 * JsonParser().parse(LObjectToString).getAsJsonObject();
		 * System.out.println(obj);
		 */

		return json;

	}

	public String fallbackUser(String phoneNum) {

		JsonObject responseObj = new JsonObject();
		responseObj.addProperty("Status", "Failure");
		responseObj.addProperty("Message", "Oops itseems to be the server is down");

		return ApplicationConstants.HYSTERIX;
	}

	public String updateUserDetails(UserPOJO userEntity) {
		String PhNo = userEntity.getPhoneno();
		List<UserPOJO> details = userRepo.getUserDetails(PhNo);
		if (details != null) {
		}

		JsonObject resObj = new JsonObject();
		resObj.addProperty("status", "success");
		resObj.addProperty("status", "successfully registerted");

		return ApplicationConstants.SUCCESS;
	}

	public String registerUserWithRestriction(UserPOJO user) {

		String phoneno = user.getPhoneno();
		List<UserPOJO> details = userRepo.getUserDetails(phoneno);
		JsonObject responseObj = new JsonObject();
		if (details.size() > 0) {
			throw new UserNotFoundException("id-" + phoneno);
		} else {
			userRepo.save(user);
			responseObj.addProperty("Status", "Success");
			responseObj.addProperty("Message", "Successfully Registered");
		}
		String responseStr = responseObj.toString();

		return responseStr;

	}

	@Override
	public String updateUserDetails(UserPOJO userEntity, String id) {
		return null;
	}

	public String updateUserMailId(String email, String phonenum) {
		List<UserPOJO> details = userRepo.findbyPhoneNum(phonenum);
		if (details.size() > 0) {
			userRepo.updateMailID(email, phonenum);
		}
		return ApplicationConstants.SUCCESS;

	}

	public Path getFileStorageLocation() {
		return fileStorageLocation;
	}

	public void setFileStorageLocation(Path fileStorageLocation) {
		this.fileStorageLocation = fileStorageLocation;
	}

	public List<UserPOJO> retriveAllUsers(String jobrole) {

		List<UserPOJO> details = getAllUsers();
		JsonObject responseObj = new JsonObject();
		
		if (jobrole.isEmpty() && jobrole != null) {
			if (details.size() > 0) {
				List<UserPOJO> newPOJO = details.stream().filter(out -> out.getJobrole().equals(jobrole))
						.collect(Collectors.toList());
				responseObj.addProperty("Status", "Success");
				return newPOJO;
			}
		}
		return details;	
	}

	@Cacheable(cacheNames = "customerList", key = "'customerList'")
	public List<UserPOJO> getAllUsers() {
		
		List<UserPOJO> userData = userRepo.getAllUsers();
		return userData;
	}

}
