package com.example.constants;

public final class ApplicationConstants {

	 private ApplicationConstants() {}

	
	public static final String SUCCESS = "SUCCESS";
	
	public static final String HYSTERIX = "Returned by Hysterix";
	
	public static final String TOKEN = "token";
	
}
