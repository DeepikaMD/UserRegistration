package com.example.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice

public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	HttpStatus httpStatus;
	
  @ExceptionHandler(Exception.class)
  public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception ex, WebRequest request) {
	  List<String> details = new ArrayList<>();
	  details.add(ex.getMessage());
	  ErrorResponse exceptionResponse = new ErrorResponse(new Date(),request.getDescription(false),details);
    return new ResponseEntity<ErrorResponse>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(UserNotFoundException.class)
  public final ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
	  List<String> details = new ArrayList<>();
	  details.add(ex.getMessage());
	  ErrorResponse exceptionResponse = new ErrorResponse(new Date(),request.getDescription(false),details);
    return new ResponseEntity<Object>(exceptionResponse, httpStatus.NOT_FOUND);
  }

	/*
	 * @ExceptionHandler(WebServiceException.class) public final
	 * ResponseEntity<Object> handleWebServiceException(HttpServletRequest request,
	 * Exception ex) { List<String> details = new ArrayList<>();
	 * details.add(ex.getMessage()); ExceptionStringResponse error = new
	 * ExceptionStringResponse(CommonConstants.SERVER_UNAVAILABLE,
	 * ex.getLocalizedMessage(), CommonConstants.STATUS_FAILURE); return new
	 * ResponseEntity(error, HttpStatus.SERVICE_UNAVAILABLE); }
	 */
  
  @ExceptionHandler(UserException.class)
  public final ResponseEntity<ErrorResponse> handleUserException(UserException ex , WebRequest request){
	  List<String> details = new ArrayList<>();
		details.add(ex.getMessage());
		
	  ErrorResponse exceptionResponse = new ErrorResponse(new Date(),request.getDescription(false),details);
	  
	  return new ResponseEntity<ErrorResponse>(exceptionResponse, HttpStatus.BAD_REQUEST);
  }
}