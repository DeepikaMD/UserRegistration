package com.example.exception;

/*@ResponseStatus(HttpStatus.NO_CONTENT)*/
public class UserException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public UserException(String details) {
	 super(details);
	}

	
}
