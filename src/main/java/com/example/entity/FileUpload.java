package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "file")
public class FileUpload {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="resume")
	@Lob
    private byte[] resume;
	
	@Column(name="phoneNo")
	private String phNum;
	
	public String getPhNum() {
		return phNum;
	}
	public void setPhNum(String phNum) {
		this.phNum = phNum;
	}
	public byte[] getResume() {
		return resume;
	}
	public void setResume(byte[] file) {
		this.resume = file;
	}
}
