package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "USER")
@ApiModel(description = "All details about the Employee. ")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserPOJO {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "Database generated employee ID")
	private int id;
	@ApiModelProperty(notes = "The employee first name")
	@Column(name = "firstName")
	private String firstName;
	@ApiModelProperty(notes = "The employee last name")
	@Column(name = "lastName")
	private String lastName;
	@ApiModelProperty(notes = "The employee address")
	@Column(name = "address")
	private String address;
	@ApiModelProperty(notes = "LoginName of employee")
	
	@Column(name = "username")
	private String username;
	@ApiModelProperty(notes = "The employee password")
	@Column(name = "password")
	private String password;
	@ApiModelProperty(notes = "The employee phone no")
	@Column(name = "phoneno")
	private String phoneno;
	@ApiModelProperty(notes = "The employee email")
	@Column(name = "email")
	private String email;
	@ApiModelProperty(notes = "The employee jobrole")
	@Column(name = "jobrole")
	private String jobrole;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getJobrole() {
		return jobrole;
	}

	public void setJobrole(String jobrole) {
		this.jobrole = jobrole;
	}
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserPOJO(int id, String firstName, String lastName, String address, String username, String password,
			String phoneno, String email, String jobrole) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.username = username;
		this.password = password;
		this.phoneno = phoneno;
		this.email = email;
		this.jobrole = jobrole;
	}

	public UserPOJO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "UserPOJO [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address
				+ ", username=" + username + ", password=" + password + ", phoneno=" + phoneno + ", email=" + email
				+ ", jobrole=" + jobrole + "]";
	}
	
	

}
