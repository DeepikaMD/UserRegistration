package com.example.exceptionHandler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.serviceImpl.UserException;
import com.example.serviceImpl.UserNotFoundException;

@ControllerAdvice
@RestController
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	HttpStatus httpStatus;
	
	
	
  @ExceptionHandler(Exception.class)
  public final ResponseEntity<ErrorResponse> handleAllExceptions(Exception ex, WebRequest request) {
    ErrorResponse exceptionResponse = new ErrorResponse(new Date(), ex.getMessage(),
        request.getDescription(false),httpStatus.BAD_GATEWAY);
    return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(UserNotFoundException.class)
  public final ResponseEntity<ErrorResponse> handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
	  ErrorResponse exceptionResponse = new ErrorResponse(new Date(), ex.getMessage(),
        request.getDescription(false),httpStatus.BAD_REQUEST);
    return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  
  @ExceptionHandler(UserException.class)
  public final ResponseEntity<ErrorResponse> handleUserException(UserException ex , WebRequest request){
	
	  ErrorResponse exceptionResponse = new ErrorResponse(new Date(), ex.getMessage(), request.getDescription(false), HttpStatus.NO_CONTENT);
	  
	  return new ResponseEntity<>(exceptionResponse,exceptionResponse.getStatus());
  }
}