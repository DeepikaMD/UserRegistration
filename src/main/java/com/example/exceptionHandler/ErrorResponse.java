package com.example.exceptionHandler;

import java.util.Date;

import org.springframework.http.HttpStatus;

public class ErrorResponse {
	
		  private Date timestamp;
		  private String message;
		  private String details;
		  private HttpStatus status;

		  public ErrorResponse(Date timestamp, String message, String details ,HttpStatus badGateway) {
		    super();
		    this.timestamp = timestamp;
		    this.message = message;
		    this.details = details;
		    this.status = badGateway;
		  }

		  public Date getTimestamp() {
		    return timestamp;
		  }

		  public String getMessage() {
		    return message;
		  }

		  public String getDetails() {
		    return details;
		  }

		public HttpStatus getStatus() {
			return status;
		}

		
		
}
