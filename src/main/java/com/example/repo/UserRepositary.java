package com.example.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.FileUpload;
import com.example.entity.UserPOJO;

@Repository
public interface UserRepositary extends CrudRepository<UserPOJO, String> {

	@Query(value= "SELECT * FROM USER WHERE phoneno=?1", nativeQuery = true)
	public List<UserPOJO> findbyPhoneNum(@Param("phoneno")String phoneNum);
	
	@Query(value = "SELECT *FROM USER WHERE phoneno=?1",nativeQuery = true)
	public List<UserPOJO> getUserDetails(String phNum);

	public void save(FileUpload fileUp);
	
	@Query(value="SELECT * FROM USER", nativeQuery = true)
	public List<UserPOJO> getAllUsers();
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="UPDATE USER SET EMAIL =?1 WHERE PHONENO=?2", nativeQuery = true)
	public void updateMailID(String email,String phNum);
	
	/*
	 * @Query(value = "UPDATE RESUME set RESUME = ?1 FROM USER WHERE PHONE_NO=?2 ",
	 * nativeQuery = true) public String updateResume(byte[] resume , String phNum);
	 */
	
	
}
